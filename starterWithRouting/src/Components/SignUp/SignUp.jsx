import React, { useState } from 'react';
import { HANDS } from '../../js/constants/hands.js';
import FormComponentText from './FormComponentText';
import FormComponentDropDown from './FormComponentDropDown';
import FormComponentRadio from './FormComponentRadio';



const SignUp = props => {
    const [signupFormTitle, setsignupFormTitle] = useState(``);
    const [signupFormFirstName, setsignupFormFirstName] = useState(``);
    const [signupFormLastName, setsignupFormLastName] = useState(``);
    const [signupFormEmail, setsignupFormEmail] = useState(``);
    const [signupFormPhoneNumber, setsignupFormPhoneNumber] = useState(``);
    const [signupFormDOB, setsignupFormDOB] = useState(null);
    const [signupFormGender, setsignupFormGender] = useState(``);

    {/* const handleSubmit = event => {
        event.preventDefault();
        props.submitsignup(signupFormTitle, signupFormFirstName, signupFormLastName, signupFormEmail,
            signupFormPhoneNumber, signupFormDOB, signupFormGender);
        setsignupFormTitle(``);
        setsignupFormFirstName(``);
        setsignupFormLastName(``);
        setsignupFormEmail(``);
        setsignupFormPhoneNumber(``);
        setsignupFormDOB(null);
        setsignupFormGender(``);
    };

    const handleTitleChange = event => {
        setsignupFormTitle(event.target.value)   
}; */}

    const handleFirstNameChange = event => {
        setsignupFormFirstName(event.target.value)
    };
    {/*}
    const handleLastNameChange = event => {
        setsignupFormLastName(event.target.value)
    };

    const handleEmailChange = event => {
    setsignupFormEmailChange(event.target.value)
    };

    const setDOBChange = event => {
        I have no clue whether this is the same or not
    }

    const handlePhoneNumberChange = event => {
    setsignupFormPhoneNumber(event.target.value)
    };

    const handleGenderChange = event => {
    setsignupFormGender(event.target.value)
    };

*/}


    return (
        <div className="container ">
            <div className="container">
                <img className='w-50' src={HANDS.src} alt={HANDS.alt} />
            </div>

            <form /*onSubmit={handleSubmit}*/ className="form centered container">
                <div><FormComponentDropDown label="Title *"
                    placeholder="Select Title..."
                    id="signupFormTitle"
                    required />
                </div>

                <FormComponentText
                    className="form-group row"
                    label="First Name *"
                    type="text" id="signupFormFirstName"
                    placeholder="First Name"
                    value={signupFormFirstName}
                    required
                    onChange={event => setsignupFormFirstName(event.target.value)}
                />

                <FormComponentText
                    className="form-group row"
                    label="Last Name *"
                    type="text"
                    id="signupFormLastName"
                    placeholder="Last Name"
                    required />

                <FormComponentText
                    className="form-group row"
                    label="Email Address *"
                    type="email"
                    id="signupFormEmail"
                    placeholder="Email Address"
                    required />

                <FormComponentText
                    className="form-group row"
                    label="Date of Birth"
                    type="date"
                    id="signupFormDOB"
                />

                <FormComponentText
                    className="form-group row"
                    label="Telephone Number"
                    type="text" pattern="[0-9]*"
                    id="signupFormPhoneNumber"
                    placeholder="Telephone Number"
                />
                <div>
                    <FormComponentRadio name="signupFormGender"
                        id="signupFormGenderMale"
                        value="Male"
                        label="Male"
                    />
                    <FormComponentRadio name="signupFormGender"
                        id="signupFormGenderFemale"
                        value="Female"
                        label="Female"
                    />
                </div>


                {/* Other and Prefer not to say options can be added here*/}

                <div className="form-group btn btn-submit">
                    <input type="submit"
                        name="submitButton"
                        id="submit"
                        value="Sign me up!">
                    </input>
                </div>
            </form >
        </div >

    );
};


export default SignUp;