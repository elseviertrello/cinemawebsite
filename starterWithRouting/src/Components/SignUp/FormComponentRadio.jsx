import React from 'react';

const FormComponentRadio = props => {
    return (
        <div className="form-check">
            <input className="form-check-input"
                type="radio"
                name={props.name}
                id={props.id}
                value={props.value}>
            </input>
            <label className="form-check-label" htmlFor={props.id} >
                {props.label}</label>
        </div>
    );
};

export default FormComponentRadio