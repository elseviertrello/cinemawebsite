import React, { useState, useEffect } from "react";
import axios from "axios";
import Locations from '../../images/Locations.jpg';
import OpeningTimes from './OpeningTimes';
import Showings from './Showings';

// URLS for json information
const OPENINGTIMESURL = `http://localhost:4000/openingTimes`;
const ALLFILMSURL = `http://localhost:4000/allFilms`;


const Schedule = () => {

    // Set state beginning with empty array
    const [openingTimes, setOpeningTimes] = useState([]);
    const [allFilms, setAllFilms] = useState([]);

    // get opening times data
    useEffect(() => {
       
        const getOpeningTimes = async () => {
            try {
                const res = await axios.get(OPENINGTIMESURL);
                const openingTimes = await res.data;
                setOpeningTimes(openingTimes);
            }
            catch (e) {
                setOpeningTimes(e.message);
            }
        }

        getOpeningTimes() ;

    }, []);

   // get film data 
    useEffect(() => {
       
        const getAllFilms = async () => {
            try {
            const res = await axios.get(ALLFILMSURL);
            const allFilms = await res.data;
            setAllFilms(allFilms);
            }
            catch (e) {
            setAllFilms(e.message);
            }
        }

        // setTimeout(() => {getAllFilms()}, 2000) ;
        getAllFilms();

    }, []);
    
    // console.log (openingTimes);
    // console.log (allFilms);

    // map data to opening times component
    const openingtimesRows = openingTimes.map(Day =>
        <OpeningTimes openingtimes={Day} key={Day._id} />
    );

    // map data to showings component
    const showingsRows = allFilms.map(FilmsOn =>
        <Showings showings={FilmsOn} key={FilmsOn._id} />
    );

    return (<>
        
        <div className="container">
            <img src={Locations} className="w-100" alt="Locations.jpg"></img>
        </div>
        
        <div className="container my-3">
            <table className="table table-bordered">
                <tbody>
                    {/* Call opening times constant function */}
                    {openingtimesRows}
                </tbody>
            </table>
        </div>
        <div className="container my-3">
            <h3>What's On</h3>
            <table className="table table-bordered">
                <tbody>
                    {/* Call showings constant function */}
                    {showingsRows}
                </tbody>
            </table>
        </div>
   
     </>
    );
}

export default Schedule;