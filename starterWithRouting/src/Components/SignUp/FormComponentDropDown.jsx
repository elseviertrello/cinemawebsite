import React from 'react';

const FormComponentDropDown = props => {
    return (
        <div className="form-group row">
            <label htmlFor={props.id} className="col-sm-2 col-form-label">{props.label}</label>
            <div className="col-sm-4">
                <select type="drop-down"
                    id={props.id}
                    placeholder={props.placeholder}
                    className="form-control"
                    required={props.required}
                //onChange = {handleTitleChange}
                >
                    <option>Mr</option>
                    <option>Mrs</option>
                    <option>Miss</option>
                    <option>Ms</option>
                    <option>Sir</option>
                    <option>Dr</option>
                    <option>Other</option></select>
            </div >
        </div >
    )
};

export default FormComponentDropDown;